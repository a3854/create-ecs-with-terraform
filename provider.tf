provider "aws" {
  region     = "eu-west-2"
  access_key = var.aws_access_key_id
  secret_key = var.aws_secret_access_key
}


terraform {
  backend "remote" {
    organization = "greatdev"

    workspaces {
      name = "create-ecs-with-terraform"
    }
  }
}
